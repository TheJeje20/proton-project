![ENSEA Logo](./Resources/ENSEA_Logo.png)
![GNU GPL Logo](./Resources/GPLV3.png)
![ARES Logo](./Resources/Logo_ARES.png)
![Astrum Logo](./Resources/Logo_Astrum.png)

# Proton project

This 4-months school project aims to land a water rocket (ENSEA - National French Engineering School).
![SpaceX Falcon Heavy rockets landing][Falcon_Heavy]

Only 1 axis is considered at first (the Z axis).
The water rocket is strapped on a wooden board and dropped from a height of 8m.


The goal is to make it land automatically, thanks to an onboard accelerometer and laser sensor.

A STM32F4-DISC1 (STM32F429ZIT6) development board is embedded to control the rocket's power based on the sensors.

The Proton project is open-source and open-hardware. It is not complete yet.

A project report and some information are available on the [wiki](https://framagit.org/TheJeje20/proton-project/wikis/home).

Happy hacking!

[Falcon_Heavy]: https://www.numerama.com/content/uploads/2018/02/25254688767_59603ff06c_o.jpg "SpaceX Falcon Heavy rockets landing"