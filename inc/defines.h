 /*
 * defines.h
 *
 *  Created on: 21 oct. 2018
 *      Author: jerome
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define I2C_CR2_TXDMAEN		I2C_CR2_DMAEN

/*
 * MPU has the following pinout:
 * SCL: PB8
 * SDA: PB9
 * INT: PB7
 * */
#define	MPU6050_I2C				I2C1
#define MPU6050_I2C_PINSPACK	TM_I2C_PinsPack_2

#define MPU6050_INT_PIN			GPIO_PIN_7	//GPIO B

/*
 * BLE module pinout:
 * TXuP: PA9 -> RXble
 * RXuP: PA10 -> TXble
 * */
#define BLE_UART_CHANNEL		USART1
#define BLE_UART_PINOUT			TM_USART_PinsPack_1
#define BLE_BAUD_SPEED			115200
#define BLE_COMMAND_OR_DATA_PIN	GPIO_PIN_6	//GPIO B

/*
 * Laser Sensor is plugged at PA0
 * https://stm32f4-discovery.net/hal_api/group___t_m___a_d_c.html
 * */
#define LASERSENSOR_PIN		TM_ADC_Channel_0

/*
 * Servomotor PWM generation
 * To give a position to the servomotor, a PWM signal is applied on the signal pin
 *
 * PWM is generated using TIM9, on the PE5 pin!
 */
#define PWM_SERVO_PIN	"PE5 and TIM9"
#define FREQUENCY_PWM_SERVO	200	//in Hz



#endif /* DEFINES_H_ */
