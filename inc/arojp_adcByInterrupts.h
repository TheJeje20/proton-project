/*
 * arojp_adcByInterrupts.h
 *
 *  Created on: 7 déc. 2018
 *      Author: jerome
 */

#ifndef AROJP_ADCBYINTERRUPTS_H_
#define AROJP_ADCBYINTERRUPTS_H_

#include "tm_stm32_adc.h"

/**
 * @brief  Status enumeration
 */
typedef enum {
	AROJP_ADC_Ok = 0x00, /*!< Everything OK */
	AROJP_ADC_Error      /*!< An error has occurred */
} AROJP_Status_t;

#if !defined(ADC_CLOCK_PRESCALER)
	#define ADC_CLOCK_PRESCALER ADC_CLOCK_SYNC_PCLK_DIV8;
#endif


AROJP_Status_t AROJP_ADC_Init(ADC_TypeDef* ADCx, TM_ADC_Channel_t channel);
AROJP_Status_t AROJP_ADC_ConfigureInterrupt(ADC_TypeDef* ADCx, TM_ADC_Channel_t channel);

int computeDistance (int adcConv);


#endif /* AROJP_ADCBYINTERRUPTS_H_ */
