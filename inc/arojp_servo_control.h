/*
 * arojp_servo_control.h
 *
 *  Created on: 26 déc. 2018
 *      Author: jerome
 */

#ifndef AROJP_SERVO_CONTROL_H_
#define AROJP_SERVO_CONTROL_H_

#include "stm32fxxx_hal.h"

TIM_HandleTypeDef hTimPWM;
TIM_OC_InitTypeDef sConfigOC;
int maxPeriodValue;

/**
 * @brief  Status enumeration
 */
typedef enum {
	AROJP_Servo_Ok = 0x00, /*!< Everything OK */
	AROJP_Servo_Error      /*!< An error has occurred */
} AROJP_ServoControl_Status_t;


/*
 * AROJP_servoPWMInit:
 * This function is meant to setup the PWM using TIM9 and PE5
 */
AROJP_ServoControl_Status_t AROJP_servoPWMInit (void);

/*
 * Sets the Duty Cycle on an initialised PWM generator
 *
 * @param dutycycle: between 0 and 100 (in percentage)
 */
AROJP_ServoControl_Status_t AROJP_Servo_setDutyCycle(int dutycycle);

/*
 * TIM9_GPIO_OutputPin_Setup:
 * This function takes the output of the Tim9-generated PWM to the GPIO PE5 pin.
 *
 * @param hTimPWM: Timer9 handle to associate to PE5
 */
void TIM9_GPIO_OutputPin_Setup(TIM_HandleTypeDef *htim);

/*
 * These 2 functions' only goal is to enable the CLK bit on TIM9 (or any timer) at initialisation.
 * They are defined into HAL as weak, thus re-defined here, since they are only used for servos.
 */
void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm);
void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* htim_pwm);


#endif /* AROJP_SERVO_CONTROL_H_ */
