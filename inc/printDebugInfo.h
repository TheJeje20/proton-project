/*
 * printDebugInfo.h
 *
 *  Created on: 11 janv. 2019
 *      Author: jerome
 */

#ifndef PRINTDEBUGINFO_H_
#define PRINTDEBUGINFO_H_

#include "stm32fxxx_hal.h"

/*
 * This function can be used to print important information to the UART connection
 */
void printDebugInformation(char* str, int number);


#endif /* PRINTDEBUGINFO_H_ */
