/*
 * arojp_HC05.h
 *
 *  Created on: 26 déc. 2018
 *      Author: jerome
 */

#ifndef AROJP_HC05_H_
#define AROJP_HC05_H_


#include "tm_stm32_usart.h"


void AROJP_USART_InitHC05(void);


#endif /* AROJP_HC05_H_ */
