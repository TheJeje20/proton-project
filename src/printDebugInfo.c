/*
 * printDebugInfo.c
 *
 *  Created on: 11 janv. 2019
 *      Author: jerome
 */

#include "printDebugInfo.h"

void printDebugInformation(char* str, int number){
	static char a [50];

	sprintf(a, "Debug: %s = %d\r\n", str, number);
	TM_USART_Puts(BLE_UART_CHANNEL, a);
}



