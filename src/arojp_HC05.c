/*
 * arojp_HC05.c
 *
 *  Created on: 26 déc. 2018
 *      Author: jerome
 */

#include "arojp_HC05.h"

void AROJP_USART_InitHC05(void){
	//Prepare the output pin to switch between data mode and command mode
	TM_GPIO_Init(GPIOB, BLE_COMMAND_OR_DATA_PIN, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_DOWN, TM_GPIO_Speed_Low);

	//***********COMMAND MODE***********
	TM_GPIO_SetPinHigh(GPIOB, BLE_COMMAND_OR_DATA_PIN); //Asks the HC-05 to take commands
	HAL_Delay(100);

	//TODO



	//************DATA MODE*************
	TM_GPIO_SetPinLow(GPIOB, BLE_COMMAND_OR_DATA_PIN); //Asks the HC-05 to transmit data without interpreting it
	HAL_Delay(100);

	TM_USART_Puts(BLE_UART_CHANNEL, "*************Begin Transmission**********\r\n");
}


