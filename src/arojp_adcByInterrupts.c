/*
 * arojp_adcByInterrupts.c
 * Only works for STM32F4ZI
 *
 *  Created on: 7 déc. 2018
 *      Author: jerome
 */

#include "arojp_adcByInterrupts.h"

/********************Copy-paste of TM_stm32_adc private functions******************/
/* Private functions */
static void TM_ADC_INT_Channel_0_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_1_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_2_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_3_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_4_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_5_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_6_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_7_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_8_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_9_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_10_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_11_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_12_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_13_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_14_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_Channel_15_Init(ADC_TypeDef* ADCx);
static void TM_ADC_INT_InitPin(GPIO_TypeDef* GPIOx, uint16_t PinX);
/*******************End of copy-paste*********************/

/* Private (interrupt shared) variable */
ADC_HandleTypeDef AdcHandle;


AROJP_Status_t AROJP_ADC_Init(ADC_TypeDef* ADCx, TM_ADC_Channel_t channel) {
	ADC_InitTypeDef initTD_Config;
	TM_ADC_Channel_t ch = (TM_ADC_Channel_t) channel;

	if (ch == TM_ADC_Channel_0) {
		TM_ADC_INT_Channel_0_Init(ADCx);
	} else if (ch == TM_ADC_Channel_1) {
		TM_ADC_INT_Channel_1_Init(ADCx);
	} else if (ch == TM_ADC_Channel_2) {
		TM_ADC_INT_Channel_2_Init(ADCx);
	} else if (ch == TM_ADC_Channel_3) {
		TM_ADC_INT_Channel_3_Init(ADCx);
	} else if (ch == TM_ADC_Channel_4) {
		TM_ADC_INT_Channel_4_Init(ADCx);
	} else if (ch == TM_ADC_Channel_5) {
		TM_ADC_INT_Channel_5_Init(ADCx);
	} else if (ch == TM_ADC_Channel_6) {
		TM_ADC_INT_Channel_6_Init(ADCx);
	} else if (ch == TM_ADC_Channel_7) {
		TM_ADC_INT_Channel_7_Init(ADCx);
	} else if (ch == TM_ADC_Channel_8) {
		TM_ADC_INT_Channel_8_Init(ADCx);
	} else if (ch == TM_ADC_Channel_9) {
		TM_ADC_INT_Channel_9_Init(ADCx);
	} else if (ch == TM_ADC_Channel_10) {
		TM_ADC_INT_Channel_10_Init(ADCx);
	} else if (ch == TM_ADC_Channel_11) {
		TM_ADC_INT_Channel_11_Init(ADCx);
	} else if (ch == TM_ADC_Channel_12) {
		TM_ADC_INT_Channel_12_Init(ADCx);
	} else if (ch == TM_ADC_Channel_13) {
		TM_ADC_INT_Channel_13_Init(ADCx);
	} else if (ch == TM_ADC_Channel_14) {
		TM_ADC_INT_Channel_14_Init(ADCx);
	} else if (ch == TM_ADC_Channel_15) {
		TM_ADC_INT_Channel_15_Init(ADCx);
	}

	#if defined(ADC1)
		if (ADCx == ADC1) {
			__HAL_RCC_ADC1_CLK_ENABLE();
		}
	#endif
	#if defined(ADC2)
		if (ADCx == ADC2) {
			__HAL_RCC_ADC2_CLK_ENABLE();
		}
	#endif
	#if defined(ADC3)
		if (ADCx == ADC3) {
			__HAL_RCC_ADC3_CLK_ENABLE();
		}
	#endif

	// From Hal :
	/*(#)Initialize the ADC low level resources by implementing the HAL_ADC_MspInit(): -> Done in HAL_Init()
       (##) Enable the ADC interface clock using __HAL_RCC_ADC_CLK_ENABLE() -> Done in TM_ADC_InitADC()
       (##) ADC pins configuration
             (+++) Enable the clock for the ADC GPIOs using the following function:
                   __HAL_RCC_GPIOx_CLK_ENABLE()	-> Done in TM_GPIO_Init() via a home-made function
             (+++) Configure these ADC pins in analog mode using HAL_GPIO_Init() -> Done in TM_GPIO_Init() - home-made
       (##) In case of using interrupts (e.g. HAL_ADC_Start_IT())
             (+++) Configure the ADC interrupt priority using HAL_NVIC_SetPriority()
             (+++) Enable the ADC IRQ handler using HAL_NVIC_EnableIRQ()
             (+++) In ADC IRQ handler, call HAL_ADC_IRQHandler()
       (##) In case of using DMA to control data transfer (e.g. HAL_ADC_Start_DMA())
             (+++) Enable the DMAx interface clock using __HAL_RCC_DMAx_CLK_ENABLE()
             (+++) Configure and enable two DMA streams stream for managing data
                 transfer from peripheral to memory (output stream)
             (+++) Associate the initialized DMA handle to the CRYP DMA handle
                 using  __HAL_LINKDMA()
             (+++) Configure the priority and enable the NVIC for the transfer complete
                 interrupt on the two DMA Streams. The output stream should have higher
                 priority than the input stream.
	*/

	initTD_Config.ClockPrescaler = ADC_CLOCK_PRESCALER;
	initTD_Config.Resolution = ADC_RESOLUTION_12B;
	initTD_Config.DataAlign = ADC_DATAALIGN_RIGHT;
	initTD_Config.ScanConvMode = DISABLE;
	initTD_Config.ContinuousConvMode = ENABLE;
	initTD_Config.DiscontinuousConvMode = DISABLE;
	initTD_Config.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	initTD_Config.ExternalTrigConv = ADC_SOFTWARE_START;
	initTD_Config.DMAContinuousRequests = DISABLE;
	initTD_Config.NbrOfDiscConversion = 0;
	initTD_Config.NbrOfConversion = 1;
	initTD_Config.EOCSelection = EOC_SEQ_CONV;

	/* Set handle */
	AdcHandle.Instance = ADCx;
	AdcHandle.Init = initTD_Config;

	/* Init ADC */
	if (HAL_ADC_Init(&AdcHandle) != HAL_OK){
		return AROJP_ADC_Error;
	}

	/* Configure ADC Interrupts */
	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	HAL_NVIC_SetPriority(ADC_IRQn, 2, 0); //ADC_IRQn is the VTOR table position of ADC
	HAL_NVIC_EnableIRQ(ADC_IRQn);

	return AROJP_ADC_Ok;
}

AROJP_Status_t AROJP_ADC_ConfigureInterrupt(ADC_TypeDef* ADCx, TM_ADC_Channel_t channel) {
	ADC_ChannelConfTypeDef sConfig;

	#if defined(ADC1)
		if (ADCx == ADC1) {
			if (! __HAL_RCC_ADC1_IS_CLK_ENABLED()) return AROJP_ADC_Error;
		}
	#endif
	#if defined(ADC2)
		if (ADCx == ADC2) {
			if (! __HAL_RCC_ADC2_IS_CLK_ENABLED()) return AROJP_ADC_Error;
		}
	#endif
	#if defined(ADC3)
		if (ADCx == ADC3) {
			if (! __HAL_RCC_ADC3_IS_CLK_ENABLED()) return AROJP_ADC_Error;
		}
	#endif

	/* Configure ADC regular channel */
	sConfig.Channel = (uint8_t) channel;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_84CYCLES; //15 cycles minimum
	sConfig.Offset = 0;

	/* Return zero */
	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK) {
		return AROJP_ADC_Error;
	}

	/* Start conversion */
	if (HAL_ADC_Start_IT(&AdcHandle) != HAL_OK) {
		return AROJP_ADC_Error;
	}

	/* Return zero */
	return AROJP_ADC_Ok;
}

/*
 * @brief
 * This function computes the distance in cm based on the ADC conversion result,
 * given the curve of the datasheet.
 *
 * @param adcConv Value converted by the ADC [0-4096]
 *
 * @return int Distance to the object in mm
 */
int computeDistance (int adcConv){
	/*
	 * First, convert the ADC result to Volts:
	 * 4096 -> 3V
	 * 0 -> 0V
	 * => 1365,33 lsb/V
	 *
	 * Then, the inverse distance is proportional to the tension
	 * U = 142.85 *1/l +1.07
	 * -> l = 142,85 *1/(U-1.07)
	 */
	return (10*142.85/(double)(adcConv/1365.33-1.07));
}



/********************Copy-paste of TM_stm32_adc private functions******************/
/* Private functions */
static void TM_ADC_INT_Channel_0_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_0);
}
static void TM_ADC_INT_Channel_1_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_1);
}
static void TM_ADC_INT_Channel_2_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_2);
}
static void TM_ADC_INT_Channel_3_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_3);
}
static void TM_ADC_INT_Channel_4_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_4);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_6);
	}
#endif
}
static void TM_ADC_INT_Channel_5_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_5);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_7);
	}
#endif
}
static void TM_ADC_INT_Channel_6_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_6);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_8);
	}
#endif
}
static void TM_ADC_INT_Channel_7_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOA, GPIO_PIN_7);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_9);
	}
#endif
}
static void TM_ADC_INT_Channel_8_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOB, GPIO_PIN_0);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_10);
	}
#endif
}
static void TM_ADC_INT_Channel_9_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOB, GPIO_PIN_1);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_11);
	}
#endif
}
static void TM_ADC_INT_Channel_10_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOC, GPIO_PIN_0);
}
static void TM_ADC_INT_Channel_11_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOC, GPIO_PIN_1);
}
static void TM_ADC_INT_Channel_12_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOC, GPIO_PIN_2);
}
static void TM_ADC_INT_Channel_13_Init(ADC_TypeDef* ADCx) {
	TM_ADC_INT_InitPin(GPIOC, GPIO_PIN_3);
}
static void TM_ADC_INT_Channel_14_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOC, GPIO_PIN_4);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_4);
	}
#endif
}
static void TM_ADC_INT_Channel_15_Init(ADC_TypeDef* ADCx) {
	if (ADCx == ADC1
#if defined(ADC2)
		|| ADCx == ADC2
#endif
	) {
		TM_ADC_INT_InitPin(GPIOC, GPIO_PIN_5);
	}
#if defined(ADC3) && defined(GPIOF)
	if (ADCx == ADC3) {
		TM_ADC_INT_InitPin(GPIOF, GPIO_PIN_5);
	}
#endif
}

static void TM_ADC_INT_InitPin(GPIO_TypeDef* GPIOx, uint16_t PinX) {
	/* Enable GPIO pin */
	TM_GPIO_Init(GPIOx, PinX, TM_GPIO_Mode_AN, TM_GPIO_OType_PP, TM_GPIO_PuPd_DOWN, TM_GPIO_Speed_Medium);
}
