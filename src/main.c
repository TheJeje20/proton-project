/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


//#include "stm32f4xx.h"
//#include "stm32f429i_discovery.h"
#include "tm_stm32_disco.h"
#include "stm32fxxx_hal.h"
#include "tm_stm32_mpu6050.h"
#include "tm_stm32_exti.h"
#include "tm_stm32_gpio.h"
#include "tm_stm32_delay.h"
#include "arojp_HC05.h"

#include "arojp_adcByInterrupts.h"
#include "arojp_servo_control.h"
#include "printDebugInfo.h"

#define LCD_USE_STM32F429_DISCOVERY


// Global variables - Caution: they are updated by interrupts
int laserSensor;		
TM_MPU6050_t myMPU;
TM_DELAY_Timer_t* sendDataToHC05_Timer;
TM_DELAY_Timer_t* reactLoop_Timer;
unsigned long relTime = 0;

//Prototypes of Callback Functions
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);
void sendDataToHC05_Callback(TM_DELAY_Timer_t* SWTIM, void* UserParameters);
void reactLoop_Callback(TM_DELAY_Timer_t* SWTIM, void* UserParameters);


int main(void)
{
	// Init system clock for maximum system speed
    TM_RCC_InitSystem();
    // Clock settings are in tm_stm32_rcc.h
    // To re-define them, just overwrite the default values of the PLLs in define.h

	HAL_Init();

	// Initial configuration of the BLE module
	TM_USART_Init(BLE_UART_CHANNEL, BLE_UART_PINOUT, BLE_BAUD_SPEED);
	AROJP_USART_InitHC05();

	TM_LCD_Init();

	if (TM_MPU6050_Init(&myMPU, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_2G, TM_MPU6050_Gyroscope_250s) != TM_MPU6050_Result_Ok) {
		printDebugInformation("Check the MPU 6050 connection", NULL);
		_Error_Handler();
	}

	if (TM_EXTI_Attach(GPIOB, GPIO_PIN_7, TM_EXTI_Trigger_Rising) != TM_EXTI_Result_Ok)	_Error_Handler();
	if (TM_MPU6050_EnableInterrupts(&myMPU) != TM_MPU6050_Result_Ok) _Error_Handler();

	AROJP_ADC_Init(ADC1, LASERSENSOR_PIN);
	AROJP_ADC_ConfigureInterrupt(ADC1, LASERSENSOR_PIN);

	// Every 10ms, Send data to the computer in order to trace graphs
	sendDataToHC05_Timer = TM_DELAY_TimerCreate(10, 1, 1, &sendDataToHC05_Callback, NULL);

	// Every 5ms, calculate the reaction of the system based on the global variables
	reactLoop_Timer = TM_DELAY_TimerCreate(5, 1, 1, &reactLoop_Callback, NULL);


	if (AROJP_servoPWMInit() != AROJP_Servo_Ok) {
		printDebugInformation("Servo PWM Initialisation error", NULL);
		_Error_Handler();
	}

	AROJP_Servo_setDutyCycle(80);

	for(;;){
	}
}


void _Error_Handler(char *file, int line)
{
  while(1)
  {
  }
}


/***************Interrupts****************/
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  /* NOTE : Redefinition of this HAL weak function */
	laserSensor = HAL_ADC_GetValue(hadc);
}

void sendDataToHC05_Callback(TM_DELAY_Timer_t* SWTIM, void* UserParameters){
	char time [12], accelerometer [6], distance [6], pwmDutyCycle [6];
	char wrap [20];

	sprintf(time,"%u",relTime);
	sprintf(accelerometer,"%d",(myMPU.Accelerometer_Y));
	sprintf(distance,"%d",computeDistance(laserSensor));
	sprintf(pwmDutyCycle,"N/A");

	strcpy(wrap, time); strcat(wrap, ", ");
	strcat(wrap, accelerometer); strcat(wrap, ", ");
	strcat(wrap, distance); strcat(wrap, ", ");
	strcat(wrap, pwmDutyCycle); strcat(wrap, "\r\n");

	TM_USART_Puts(BLE_UART_CHANNEL, wrap);
	AROJP_Servo_setDutyCycle(100.0/4096*laserSensor);
}

void reactLoop_Callback(TM_DELAY_Timer_t* SWTIM, void* UserParameters){
	relTime += 5;
}
