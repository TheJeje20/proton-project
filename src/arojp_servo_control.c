/*
 * arojp_servo_control.c
 *
 *  Created on: 26 déc. 2018
 *      Author: jerome
 *      Based on https://stm32f4-discovery.net/2014/05/stm32f4-stm32f429-discovery-pwm-tutorial/
 */

#include "arojp_servo_control.h"


AROJP_ServoControl_Status_t AROJP_servoPWMInit (void){
	__HAL_RCC_GPIOE_CLK_ENABLE();


	//TIM9 is connected to APB2 bus (45MHz max)
	hTimPWM.Instance = TIM9;
	/*
		Set timer period when it have reset
		First you have to know max value for timer
		In our case it is 16bit = 65535

		If you know your PWM frequency you want to have timer period set correct

		TIM_Period = timer_tick_frequency / PWM_frequency - 1

		In this case, PWM_frequency is desired to be 200Hz.
	*/
	hTimPWM.Init.Prescaler = 199;
	hTimPWM.Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED3; //Counts up and down

	uint32_t apb2freq = HAL_RCC_GetPCLK2Freq();
	printDebugInformation("Frequency of the APB2 bus to generate the PWM",apb2freq);
	maxPeriodValue = (apb2freq/((hTimPWM.Init.Prescaler+1)*FREQUENCY_PWM_SERVO/2));
	printDebugInformation("PWM Period Value", maxPeriodValue);

	hTimPWM.Init.Period = maxPeriodValue - 1;
	hTimPWM.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	if (HAL_TIM_PWM_Init(&hTimPWM) != HAL_OK)
	{
		return AROJP_Servo_Error;
	}

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = maxPeriodValue/2;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&hTimPWM, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		return AROJP_Servo_Error;
	}

	TIM9_GPIO_OutputPin_Setup(&hTimPWM);

	if (HAL_TIM_PWM_Start(&hTimPWM, TIM_CHANNEL_1) != HAL_OK) {
		printDebugInformation("Servo PWM Start error",NULL);
		return AROJP_Servo_Error;
	}

	/*
	 * __HAL_TIM_GET_AUTORELOAD(&htim1); //gets the Period set for PWm
	 * __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, dutyCycle); //sets the PWM duty cycle (Capture Compare Value)
	 * */

	return AROJP_Servo_Ok;
}

void TIM9_GPIO_OutputPin_Setup(TIM_HandleTypeDef* hTimPWM){
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{
  if(htim_pwm->Instance==TIM9)
  {
    /* Peripheral clock enable */
    __HAL_RCC_TIM9_CLK_ENABLE();
  }
}

void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* htim_pwm)
{
  if(htim_pwm->Instance==TIM9)
  {
    /* Peripheral clock disable */
    __HAL_RCC_TIM9_CLK_DISABLE();
  }
}

AROJP_ServoControl_Status_t AROJP_Servo_setDutyCycle(int dutycycle){
	sConfigOC.Pulse = (int)(dutycycle/100.0*(maxPeriodValue-1));
	if (HAL_TIM_PWM_ConfigChannel(&hTimPWM, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	{
		return AROJP_Servo_Error;
	}
	if (HAL_TIM_PWM_Start(&hTimPWM, TIM_CHANNEL_1) != HAL_OK) {
		printDebugInformation("Servo PWM Start error",NULL);
		return AROJP_Servo_Error;
	}
}
