/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#ifdef USE_RTOS_SYSTICK
#include <cmsis_os.h>
#endif
#include "stm32f4xx_it.h"
#include "defines.h"
#include "tm_stm32_mpu6050.h"
#include "arojp_mpuByInterrupts.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef AdcHandle;
extern TM_MPU6050_t myMPU;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            	  	    Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles SysTick Handler, but only if no RTOS defines it.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
#ifdef USE_RTOS_SYSTICK
	osSystickHandler();
#endif
}


void ADC_IRQHandler(void){
	HAL_ADC_IRQHandler(&AdcHandle);
}

void TM_EXTI_Handler(uint16_t GPIO_Pin){
	//When the MPU finished a measure, it notifies the STM32 via its INT pin
	if (GPIO_Pin == MPU6050_INT_PIN){
		//Then, data is acquired, filtered and put into a variable
		TM_MPU6050_Result_t accelmeasure = TM_MPU6050_ReadAccelerometer(&myMPU);
	}
}
